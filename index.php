<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>OOP</title>
</head>

<body>

<?php
	require_once('animal.php');
	require('ape.php');
	require('frog.php');

	$sheep = new Animal("shaun");

	echo "Name : ".$sheep->name."<br>"; // "shaun"
	echo "Legs : ".$sheep->legs."<br>"; // 4
	echo "Cold Bloded : ".$sheep->cold_blooded."<br>"; // "no"	

	$kodok = new Frog("buduk");

	echo "<br>Name : ".$kodok->name;
	echo "<br>Legs : ".$kodok->legs;
	echo "<br>Cold Bloded : ".$kodok->cold_blooded;
	echo "<br>Jump : ";
	$kodok->jump(); // "hop hop"	

	$sungokong = new Ape("kera sakti");

	echo "<br><br>Name : ".$sungokong->name;
	echo "<br>Legs : ".$sungokong->legs;
	echo "<br>Cold Bloded : ".$sungokong->cold_blooded;
	echo "<br>Yell : ";
	$sungokong->yell(); // "Auooo"
?>

</body>
</html>